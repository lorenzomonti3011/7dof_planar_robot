#include "trajectory_generation.h"
#include <iostream>

using namespace std;

Polynomial::Polynomial(){};

Polynomial::Polynomial(const double &piIn, const double &pfIn, const double & DtIn){
  //TODO initialize the object polynomial coefficients
  pi = piIn ;
  pf = pfIn ;
  Dt = DtIn ;
  a[0] = pi ;
  a[1] = 0 ;
  a[2] = 0 ;
  a[3] = 10*(pf - pi) ;
  a[4] = -15*(pf - pi) ;
  a[5] = 6*(pf - pi) ;

};

void          Polynomial::update(const double &piIn, const double &pfIn, const double & DtIn){
  //TODO update polynomial coefficients
  pi = piIn ;
  pf = pfIn ;
  Dt = DtIn ;
  a[0] = pi;
  a[1] = 0;
  a[2] = 0;
  a[3] = 10*(pf - pi);
  a[4] = -15*(pf - pi);
  a[5] = 6*(pf - pi);

};

const double  Polynomial::p     (const double &t){
  //TODO compute position

  double p = a[0] + a[1]*(t/Dt) + a[2]*pow((t/Dt),2) + a[3]*pow((t/Dt),3) + a[4]*pow((t/Dt),4) + a[5]*pow((t/Dt),5);

  return p;
};

const double  Polynomial::dp    (const double &t){
  //TODO compute velocity
  
  double dp = a[1]/Dt + (2*a[2]/pow(Dt,2))*t + (3*a[3]/pow(Dt,3))*pow(t,2) + (4*a[4]/pow(Dt,4))*pow(t,3) + (5*a[5]/pow(Dt,5))*pow(t,4);

  return dp;
};

Point2Point::Point2Point(const Eigen::Vector3d & xi, const Eigen::Vector3d & xf, const double & DtIn){
  //TODO initialize object and polynomials

  Dt=DtIn;

  polx = Polynomial(xi(0), xf(0), Dt);
  poly = Polynomial(xi(1), xf(1), Dt);
  polz = Polynomial(xi(2), xf(2), Dt);
  
};

Eigen::Vector3d Point2Point::X(const double & time){
  //TODO compute cartesian position
 
  Eigen::Vector3d X = Eigen::Vector3d::Zero();

  X(0)= polx.p(time);
  X(1)= poly.p(time);
  X(2)= polz.p(time);

  return X;
};

Eigen::Vector3d Point2Point::dX(const double & time){
  //TODO compute cartesian velocity
  Eigen::Vector3d dX = Eigen::Vector3d::Zero();

  dX(0)= polx.dp(time);
  dX(1)= poly.dp(time);
  dX(2)= polz.dp(time);

  return dX;
};

Frame2Frame::Frame2Frame(const Eigen::Affine3d TiIn, const Eigen::Affine3d TfIn, const double & DtIn){
  //TODO initialize object and polynomials
  Eigen::Affine3d Ti = TiIn;
  Eigen::Affine3d Tf = TfIn;
  Dt=DtIn;

  Ri = Ti.rotation();
  Rf = Tf.rotation();

  polx = Polynomial(Ti.translation()(0), Tf.translation()(0), Dt);
  poly = Polynomial(Ti.translation()(1), Tf.translation()(1), Dt);
  polz = Polynomial(Ti.translation()(2), Tf.translation()(2), Dt);

  Eigen::Matrix3d Rif = Ri.transpose()*Rf;
  Eigen::AngleAxisd Rif_rot(Rif);
  axis = Rif_rot.axis();
  double angle_d = Rif_rot.angle();
  polr = Polynomial(0, angle_d, Dt);

  
};

Eigen::Affine3d Frame2Frame::T(const double & time){

  Eigen::Affine3d T;
  T.linear() = Eigen::AngleAxisd(polr.p(time),axis).matrix();
  T.translation() = Eigen::Vector3d(polx.p(time), poly.p(time), polz.p(time));

  return  T;

}

Eigen::Matrix<double,6,1> Frame2Frame::dT(const double & time){

  Eigen::Matrix<double,6,1> dT;
  
  dT.block<3,1>(0,0) = Eigen::Vector3d(polx.dp(time), poly.dp(time), polz.dp(time));
  dT.block<3,1>(3,0) = Eigen::Vector3d(polr.dp(time)*axis);

  return  dT;

}