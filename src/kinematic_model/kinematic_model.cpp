#include "kinematic_model.h"
#include <iostream>

using namespace std;

RobotModel::RobotModel():
  d1  (0.333),
  d3  (0.316),
  a4  (0.0825),
  a5  (-0.0825),
  d5  (0.384),
  a7  (0.088),
  d8  (0.107)
{};

Eigen::Matrix4d MatrixTransformation(Eigen::Vector4d u){
     
    double u0 = u(0);
    double u1 = u(1);
    double u2 = u(2);
    double u3 = u(3);
 
    double T00 = cos(u2);
    double T01 = -sin(u2);
    double T02 = 0;
    double T03 = u1;
    
    double T10 = cos(u0)*sin(u2);
    double T11 = cos(u0)*cos(u2);
    double T12 = -sin(u0);
    double T13 = -u3*sin(u0);
    
    double T20 = sin(u0)*sin(u2);
    double T21 = sin(u0)*cos(u2);
    double T22 = cos(u0);
    double T23 = u3*cos(u0);
    
    double T30 = 0;
    double T31 = 0;
    double T32 = 0;
    double T33 = 1;
    
    Eigen::Matrix4d T;

    T << T00, T01, T02, T03,
         T10, T11, T12, T13,
         T20, T21, T22, T23,
         T30, T31, T32, T33;
   
    return T; 
  }

void RobotModel::FwdKin(Eigen::Vector3d &xOut, Eigen::Matrix<double,6,7> &JOut, const Eigen::Matrix<double,7,1> & qIn){
  // TODO Implement the forward and differential kinematics

  Eigen::Vector4d u1 (0,0,qIn(0),d1);
  Eigen::Vector4d u2 (-M_PI_2,0,qIn(1),0);
  Eigen::Vector4d u3 (M_PI_2,0,qIn(2),d3);
  Eigen::Vector4d u4 (M_PI_2,a4,qIn(3),0);
  Eigen::Vector4d u5 (-M_PI_2,a5,qIn(4),d5);
  Eigen::Vector4d u6 (M_PI_2,0,qIn(5),0);
  Eigen::Vector4d u7 (M_PI_2,a7,qIn(6),0);

  
  Eigen::Matrix4d T0_1 = MatrixTransformation(u1);
  Eigen::Matrix4d T1_2 = MatrixTransformation(u2);
  Eigen::Matrix4d T2_3 = MatrixTransformation(u3);
  Eigen::Matrix4d T3_4 = MatrixTransformation(u4);
  Eigen::Matrix4d T4_5 = MatrixTransformation(u5);
  Eigen::Matrix4d T5_6 = MatrixTransformation(u6);
  Eigen::Matrix4d T6_7 = MatrixTransformation(u7);
  
  Eigen::Matrix4d T0_2 = T0_1*T1_2;
  Eigen::Matrix4d T0_3 = T0_2*T2_3;
  Eigen::Matrix4d T0_4 = T0_3*T3_4;
  Eigen::Matrix4d T0_5 = T0_4*T4_5;
  Eigen::Matrix4d T0_6 = T0_5*T5_6;

  Eigen::Matrix4d T0_7 = T0_6*T6_7;

  Eigen::Vector4d EF (0,0,d8,1);

  Eigen::Vector4d xOut_temp = T0_7*EF;
  xOut = xOut_temp.head(3);
  
  Eigen::Vector3d P1 = T0_1.block<3,1>(0,3);//[0:3,3]
  Eigen::Vector3d P2 = T0_2.block<3,1>(0,3);
  Eigen::Vector3d P3 = T0_3.block<3,1>(0,3);
  Eigen::Vector3d P4 = T0_4.block<3,1>(0,3);
  Eigen::Vector3d P5 = T0_5.block<3,1>(0,3);
  Eigen::Vector3d P6 = T0_6.block<3,1>(0,3);
  Eigen::Vector3d P7 = T0_7.block<3,1>(0,3);

  Eigen::Vector3d z1 = T0_1.block<3,1>(0,2);//[0:3,2]
  Eigen::Vector3d z2 = T0_2.block<3,1>(0,2);
  Eigen::Vector3d z3 = T0_3.block<3,1>(0,2);
  Eigen::Vector3d z4 = T0_4.block<3,1>(0,2);
  Eigen::Vector3d z5 = T0_5.block<3,1>(0,2);
  Eigen::Vector3d z6 = T0_6.block<3,1>(0,2);
  Eigen::Vector3d z7 = T0_7.block<3,1>(0,2);
  
  Eigen::Vector3d P1_7 = P7-P1;
  Eigen::Vector3d P2_7 = P7-P2;
  Eigen::Vector3d P3_7 = P7-P3;
  Eigen::Vector3d P4_7 = P7-P4;
  Eigen::Vector3d P5_7 = P7-P5;
  Eigen::Vector3d P6_7 = P7-P6;
  Eigen::Vector3d P7_7 = P7-P7;


  Eigen::Vector3d J00 = z1.cross(P1_7);
  Eigen::Vector3d J01 = z2.cross(P2_7);
  Eigen::Vector3d J02 = z3.cross(P3_7);
  Eigen::Vector3d J03 = z4.cross(P4_7);
  Eigen::Vector3d J04 = z5.cross(P5_7);
  Eigen::Vector3d J05 = z6.cross(P6_7);
  Eigen::Vector3d J06 = z7.cross(P7_7);
  
  Eigen::Vector3d J10 = z1;
  Eigen::Vector3d J11 = z2;
  Eigen::Vector3d J12 = z3;
  Eigen::Vector3d J13 = z4;
  Eigen::Vector3d J14 = z5;
  Eigen::Vector3d J15 = z6; 
  Eigen::Vector3d J16 = z7;
  
  Eigen::Matrix<double,6,7> Jn; 
  Jn << J00, J01, J02, J03, J04, J05, J06,
        J10, J11, J12, J13, J14, J15, J16;

  double xx= T0_7(0,0);
  double xy= T0_7(1,0);
  double xz= T0_7(2,0);

  double zx= T0_7(0,2);
  double zy= T0_7(1,2);
  double zz= T0_7(2,2);
  
  Eigen::Matrix<double,6,6> D;
  
  D << 1, 0, 0, 0, a7*xz + d8*zz, -a7*xy - d8*zy, 
       0, 1 ,0, -a7*xz - d8*zz, 0, a7*xx + d8*zx, 
       0, 0 ,1, a7*xy + d8*zy, -a7*xx - d8*zx, 0, 
       0, 0, 0, 1, 0, 0, 
       0, 0, 0, 0, 1, 0, 
       0, 0, 0, 0, 0, 1;   
  
  JOut = D*Jn;
  
    
}