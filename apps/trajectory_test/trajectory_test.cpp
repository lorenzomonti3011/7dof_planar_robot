#include "trajectory_generation.h"
#include <iostream>

using namespace std;




int main(){

  // Compute the trajectory for given initial and final positions. 
  // Display some of the computed points
  // Check numerically the velocities given by the library 
  // Check initial and final velocities

  const double Dt =  3;
  const double dt =  pow(10,-6);

  Eigen::Vector3d X_i (0,0,0);
  Eigen::Vector3d X_f (1,2,1);

  Eigen::Matrix3d Ri ;

  Ri <<  1, 0, 0, 
         0, 1, 0, 
         0, 0, 1;
  Eigen::Affine3d Ti;
  Ti.linear() = Ri;
  Ti.translation() = X_i;

  Eigen::AngleAxisd Ri_rot(Ri);
  Eigen::Vector3d axis = Ri_rot.axis();
  Eigen::Matrix3d Rf = Eigen::AngleAxisd(0.5,axis).matrix();

  Eigen::Affine3d Tf;
  Tf.linear() = Rf;
  Tf.translation() = X_f;

  
  Frame2Frame myFrame2Frame(Ti, Tf,  Dt);


  Eigen::Affine3d T;
  Eigen::Matrix<double,6,1> dT;

  float t = 0;

  while(t < Dt)
  {

    T = myFrame2Frame.T(t);
    dT = myFrame2Frame.dT(t);


    cout << "R = " << T.rotation() << "\n\n\n";
    cout << "X = " << T.translation() << "\n\n\n";
    cout << "dX = " << dT << "\n\n\n";

    t = t+0.1;
    

  } 
  cout << "Rf desired =" << Tf.rotation() << "\n";
   
}