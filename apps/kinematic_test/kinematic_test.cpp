#include "kinematic_model.h"
#include <iostream>
#include <cmath> 

using namespace std;




int main(){
  
  RobotModel myRobotModel;

  Eigen::Matrix<double,7,1> q;
  Eigen::Vector3d x = Eigen::Vector3d::Zero();
  Eigen::Matrix<double,6,7> J;

  q(0) = 0;
  q(1) = 0;
  q(2) = 0;
  q(3) = 0;
  q(4) = 0;
  q(5) = 0;
  q(6) = 0; 

  myRobotModel.FwdKin(x, J, q);

  cout << "x = " << x << "\n\n";
  cout << "J = " << J << "\n\n";

}
