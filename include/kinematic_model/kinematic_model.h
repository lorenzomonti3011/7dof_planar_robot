#pragma once
#include "eigen3/Eigen/Dense"
//#include <math.h> 

//! \brief Kinematic model of a 2-DoF robot (Forward and differential)
class RobotModel{
  public:
    //! \brief Kinematic model with given link lengths 
    //!
    //! \param d1 length 1
    //! \param d3 length 2
    //! \param a4 length 2
    //! \param a5 length 2
    //! \param d5 length 2
    //! \param a7 length 2
    
    RobotModel();

    //! \brief Compute position and jacobian matrix for given joint position
    //!
    //! \param x Output cartesian position
    //! \param J Output jacobian matrix
    //! \param q joint position
    void FwdKin(Eigen::Vector3d &xOut, Eigen::Matrix<double,6,7> &JOut, const Eigen::Matrix<double,7,1> & qIn);
  private:
    const double d1   ;   // length 1
    const double d3   ;   // length 2
    const double a4   ;   // length 3
    const double a5   ;   // length 4
    const double d5   ;   // length 5
    const double a7   ;   // length 6
    const double d8   ;   // length 6
};